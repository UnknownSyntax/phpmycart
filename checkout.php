<?php
require_once 'app/bootstrap.php';
$cd = $session->getSetting("clientdata");
require_once 'app/Models/UserModel.php';
$user = new \app\Models\UserModel($database);

$template->setVar("totalprice", $session->getSetting("total"));
$template->setVar("addr", $user->country($cd['client_id']));

if(!empty($_POST)) {
	if($_POST['payment'] == 'paypal') {
		header("location: payment/paypal.php?c=".$_POST['courier']);
	}
}

if(isset($cd['client_id'])) {
	$template->display("pages/checkout", "loggedin");
} else {
	header("location: /signin.php?return=checkout.php");
}