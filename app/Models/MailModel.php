<?php
require_once 'Zend/Mail/Transport/Smtp.php';
require_once 'Zend/Mail.php';

class Mail {
	
	public function __construct() {
		
	}
	
	public function register($first,$last,$user,$email,$pass) {
		$config = array('auth' => 'login',
			'username' => SYSTEM_EMAIL,
			'password' => MAIL_PASSWD);
	
		$transport = new Zend_Mail_Transport_Smtp(MAILSERVER, $config);
		$mail = new Zend_Mail();
		$mail->addHeader('X-MailGenerator', SYSTEM_NAME);
		$mail->setBodyHTML('<div padding="15px" font-face="arial" style="background: #ccc; padding: 15px">Hello, '.$first.' '.$last.'!<br>You\'re awesome for signing up to AzureNI!<br><br>Your login details are as follows:<br><strong>Username:</strong>'.$user.'<br><strong>Password:</strong>'.$pass.'<br><br>And for reference, your email address:<br>'.$email.'.<br><br>Warm regards,<br>AzureNI Administrators<br>support@azureni.com</div></body>');
		$mail->setFrom(SYSTEM_EMAIL, SYSTEM_NAME);
		$mail->addTo($email, $first.' '.$last);
		$mail->setSubject('Your New Account Information!');
		$mail->send($transport);
	}
}