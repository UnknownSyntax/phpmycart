<?php
require_once 'app/bootstrap.php';
$cd = $session->getSetting("clientdata");
require_once 'app/Models/ProductsModel.php';
$products = new \app\Models\ProductsModel($database);

try {
	if(empty($_GET['id'])) {
		$template->setVar("allproducts", $products->fetchProducts((int)1,(int)400));
		$ap = count(json_decode($products->fetchProducts((int)1,(int)400)));
			$lastpage = ceil($ap / 10) - 1;
		if(empty($_GET)) {
			header("location: /product/page/1");
		} elseif($_GET['pg'] < 1) {
			header("location: /product/page/1");
		} elseif($_GET['pg'] > $lastpage) {
			header("location: /product/page/".$lastpage);
		} else {
			(int)$pg = $_GET['pg'];
		}
		$template->setVar("products", $products->paginate((string)$database->tbl("products"), (int)10, "product_id", $pg, (string)"fetchProducts"));
	} else {
		$template->setVar("products", $products->fetchProductById((int)$_GET['id']));
		try {
			$template->setVar("pictures", $products->fetchMorePictures((int)$_GET['id']));
		} catch (Exception $e) {
			$template->setVar("picerror", $e->getMessage());
		}
	}
} catch (Exception $e) {
	$template->setVar("errmsg", $e->getMessage());
}


if(isset($cd['client_id'])) {
	$template->display("pages/products", "loggedin");
} else {
	$template->display("pages/products", "loggedout");
}