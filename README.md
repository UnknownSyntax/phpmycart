phpMyCart
======

[![Build Status](https://travis-ci.org/ukblabberbox/phpmycart.png?branch=v1)](https://travis-ci.org/ukblabberbox/phpmycart)

phpMyCart is an open-source ecommerce website designed for beginners or the non-technical minded person.

This will be the last release that phpMyCart will see for a while.
