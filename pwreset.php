<?php
require_once 'app/bootstrap.php';

if(empty($cd['client_id'])) {
	$template->display("pages/pwreset", "loggedout");
} else {
	header("location: /account.php?a=password");
}