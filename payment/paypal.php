<?php
chdir('../');
require_once 'app/bootstrap.php';
require_once 'app/Models/PayPalModel.php';
$paypal = new \app\Models\PayPal();
require_once 'app/Models/UserModel.php';
$user = new \app\Models\UserModel($database);
$cd = $session->getSetting("clientdata");

$template->setVar("paypal", json_encode($paypal));
$template->setVar("cost", $session->getSetting("total"));
$template->setVar("items", $session->getSetting("items"));
$template->setVar("addr", $user->country($cd['client_id']));

if(isset($cd['client_id'])) {
	$template->display("pages/paypal", "loggedin");
} else {
	header("location: /signin.php");
}